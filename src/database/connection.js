const { Sequelize } = require('sequelize');

const DB = new Sequelize(env('DB_DATABASE'), env('DB_USERNAME'), env('DB_PASSWORD'), {
    host: env('DB_HOST'),
    dialect: env('DB_CONNECTION'),
    port: env('DB_PORT'),
    define: {
        freezeTableName: true,
        timestamps: false
    }
});

const connection = async () => {
    try {
        await DB.authenticate();
        console.log('Connection has been established successfully.');
      } catch (error) {
        console.error('Unable to connect to the database:', error);
      }
}
connection()

module.exports = DB