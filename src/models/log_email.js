const { Sequelize, DataTypes, Model } = require('sequelize');
const DB = require('../database/connection')

const LogEmail = DB.define('log_emails', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    }, 
    app_name: {
        type: DataTypes.STRING,
        allowNull: true,
    }, 
    subject: {
        type: DataTypes.STRING,
        allowNull: true,
    },
    from: {
        type: DataTypes.STRING,
        allowNull: true,
    },
    to: {
        type: DataTypes.STRING,
        allowNull: true,
    },
    created_at: {
        type: DataTypes.DATE, defaultValue: DataTypes.NOW 
    },
    updated_at: {
        type: DataTypes.DATE, defaultValue: DataTypes.NOW 
    },
    response: {
        type: DataTypes.STRING,
        allowNull: true,
    }
})

module.exports = LogEmail