const helper = require('../../helper')
const env = helper.env

const config = {
    from_mail: env('MAIL_FROM_ADDRESS'),
    from_name: env('MAIL_FROM_NAME'),
    host: env('MAIL_HOST', 'smtp.mailgun.org'),
    port: env('MAIL_PORT', 587) * 1,
    encryption: env('MAIL_ENCRYPTION', 'tls'),
    secureConnection: false,
    auth: {
        user: env('MAIL_USERNAME'),
        pass: env('MAIL_PASSWORD')
    },
    timeout: null,
 }

module.exports = config