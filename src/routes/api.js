var express = require('express');
var api = express.Router();
var mailer = require('./../services/mailer');
var AES = require('crypto-js/aes');

api.use(express.json())
/** Middleware for timelog */
api.use(function timeLog (req, res, next) {
    console.info(new Date(), req.url);
    next()
})


api.get('/', (req, res, next) => {
    res.send('API Homepage')
})

api.post('/send_email', (req, res, next) => {
    console.log("SENDING EMAIL")
    let config = req.body
    let response = mailer.sendEmail(config)
    res.send({
        "response": response
    });
})

api.get('/generate_secret/:secret', (req, res) => {
    console.log(req.params)
    let encrypted = AES.encrypt(req.params.secret, "UwU").toString()
    console.log(encrypted)
    let decrypted = AES.decrypt(encrypted, "UwU").toString()
    console.log(req.params.secret == decrypted)
    res.send({
        encrypted: encrypted
    });
})

module.exports = api
