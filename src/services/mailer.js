var nodemailer = require('nodemailer')
const AES = require('crypto-js/aes')
const fs = require('fs')
const LogEmail = require('../models/log_email')

const sendEmail = async ({ application, secret, email, message }) => {
    // console.log('=== inside mailer.sendEmail()')

    const clientList = JSON.parse(fs.readFileSync('./src/user/client.json', 'utf-8'))
    let client = clientList[application]
    
    if (client.secret == AES.decrypt(secret, 'uwu')) {
        throw 'Secret Key Not a Matches'
    }
    const transporter = nodemailer.createTransport(client['config'])
    var message = createMessage({
        from: `${client.config.from_name} <${client.config.from_mail}>`,
        to: email,
        subject:  message.subject,
        text: message.text,
        html: message.html,
    })

    console.time('sending_email_done')
    console.log('... sending email ...')
    let result = true
    let response;
    try {
        response = await transporter.sendMail(message)
    } catch (error) {
        console.log('error', error)
        response = error
        result = false
    }
    
    // console.log(message);
    let log = await LogEmail.build({
        app_name: application,
        from: `${client.config.from_name} <${client.config.from_mail}>`,
        to: JSON.stringify(email),
        subject:  JSON.stringify(message.subject),
        response: JSON.stringify(response)
    })
    log.save()

    console.timeEnd('sending_email_done')
    return result
}

const createMessage = ({ from, to, subject, text, html }) => {
    if (from == null) {
        from = config.from
    }
    return {
        from: from,
        to: to ?? ['malikal.j4@gmail.com'],
        subject: subject ?? 'TESTING',
        text: text ?? 'TESTING',
        html: html ?? 'TESTING',
    }
}

const mailer = {
    sendEmail: sendEmail,
}

module.exports = mailer
