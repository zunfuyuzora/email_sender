# Email Sender

#### Description
`Email Sender`  


## Installation

Run `npm install` to install necessary dependencies (Express, Nodemailer, Nodemon, Sequelize)

```
npm install
```

## Usage/Examples

#### Setup

Copy .env.example to .env file and set database information for logging 

```
$ copy .env.example .env
```
```
DB_CONNECTION=mariadb
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=carshow_local
DB_USERNAME=root
DB_PASSWORD=
```

Run local development server using `npm run dev`


This service using application name and secret key to validate the service 


Setup consumer application in src/user/client.js

```
{
    "appX" : {  // Consumer Application name 
        "secret": "XXX", // For Validation
        "config": {
            "from_mail": "admin@application.id",
            "from_name": "Application Admin",
            "host": "example.smtp.com",
            "port": 587,
            "encryption": "tls",
            "secureConnection": false,
            "auth": {
                "user": "xxx",
                "pass": "XXX"
            },
            "timeout": null
        }
    },
    ...
}
```

Generate secret key via `/api/generate_key/:secret`

```
// localhost:2727/api/generate_key/XXX
...

{
  "encrypted": "U2FsdGVkX19EIGP/R09MapJMCcANpns7jlsD5D/ESIw="
}

```
Send Email by send JSON POST request via `/api/send_email`

```
// Request Body
...
{
    "application" : "appX",
    "secret" : "U2FsdGVkX19EIGP/R09MapJMCcANpns7jlsD5D/ESIw=",
    "email" : [ "admin@mail.com", "another@mail.com" ],
    "message" : {
        'subject' : "Email Sender UwU",
        'text' : "This is email sender",
        'html' : "<h3>This is email sender</h3>"
    }
}
```

#### Example 

Setup .env file in laravel app 
```
VDL_USED=TRUE
VDL_EMAIL_SENDER=http://localhost:2727/api/send_email
VDL_APPLICATION_NAME=carshow
VDL_SECRET=U2FsdGVkX1+YemVjWkuGeD+Wy5HKQCtai15UhJ+z/8k=

```

Create or Configure an email service to process email request
```
// App\Service\Email\EmailService.php
    ...
    public static function productApprovalRequest(Product $product)
    {
        $subscriber = Email::getSubscriberOf(Email::PRODUCT_APPROVAL_REQUEST);
        if (config('app.email_sender.active')) {
        $message = [
            "subject" => "Notifikasi Email Ukyo",
            'text' => "Notifikasi Email Ukyo",
            'html' => str_replace('\r\n', '', view('email.product_approval_request', [
                "product_name" => $product->name,
                "product_showroom" => optional($product->showroom)->name ?? "Showroom",
            ])->render())
        ];
        self::send($subscriber, $message);
        } else {
            Mail::to($subscriber)->send(new ProductApprovalRequest($product));
        }
    }
    ...

    public static function send($email, $message)
    {
        $payload = [
            'application' => config('app.email_sender.name'),
            'secret' => config('app.email_sender.secret'),
            'email' => $email,
            'message' => json_encode($message)
        ];
        return Http::asJson()->post(config('app.email_sender.url'), $payload);
    }
    ...
```

Testing using `php artisan tinker`

```
// php artisan tinker
...
$ use App\Service\Email\EmailService 
$ EmailService::productApprovalRequest(Product::find(1))
```

We should have this output on our `email_sender` local server CLID

```
2022-08-15T07:46:01.789Z /send_email
SENDING EMAIL
... sending email ...
sending_email: 7.025s
```
## FAQ

#### Is it stable?

Of course not! U^U 


