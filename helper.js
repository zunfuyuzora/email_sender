require('dotenv').config()

const env = (key) => {
    if (key === "" | key === null) {
        return {};
    }
    let x = process.env[key];
    if (x !== null) {
        return x
    }
    return {}
}

const helper = {
    env: env
}

module.exports = helper