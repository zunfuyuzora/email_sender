var express = require('express');
var app = express();
global.env = require('./helper').env

var api = require('./src/routes/api')
app.use('/api/', api);


function logger(req, res, next) {
    console.log(new Date(), req.url)
    next()
}
app.use(logger)


app.get('/', (req, res) => {
    res.send('Hello World! HTTP GET request using express.js >w<)/)')
})


app.get('/ukyo', (req, res) => {
    res.send('HI! UKYO DESU!!')
})

const hello = (req, res, next) => {
    res.write('Hello Page!!')
    next()
}

const hello_again = (req, res, next) => {
    res.write('Hello again too!')
    res.end()
}

app.get('/multi-function', hello, hello_again)


var server = app.listen(env('APP_PORT'), () => {
    console.log(`Listening on Port ${env('APP_PORT')} ...`)
})